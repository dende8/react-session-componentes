import { Component } from 'react';
import { Navbar, Section, Header, Counter, Form } from './components';
import './App.scss';

const user = {
  firstName: 'Juan',
  lastName: 'González',
  age: 30,
  image: 'https://bit.ly/3e7XhiJ',
  about: 'Soy una persona que siempre está aprendiendo cosas nuevas',
  education: [
    { title: 'Ingeniero civil', company: 'Universidad de Salamanca' },
    { title: 'Máster en Big Data', company: 'Universidad de Madrid' },
  ],
  experience: [
    { title: 'Junior data scientist', company: 'Upgrade Hub' },
    { title: 'Senior data scientist', company: 'Upgrade Hub Labs' },
  ],
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: { ...user },
      isEditing: true,
    };
  }

  setIsEditing = () => {
    this.setState({
      isEditing: !this.state.isEditing,
    });
  };

  changeUserFirstName = (name) => {
    this.setState({
      user: {
        ...this.state.user,
        firstName: name,
      }
    });
  };

  render() {
    const { user } = this.state;
    return (
      <div className="app">
        {this.state.isEditing
          ? <Form user={user} changeUserFirstName={this.changeUserFirstName} />
          : <div>
            <Navbar />
            <Counter startCount={0} />
            <Header fullName={`${user.firstName} - ${user.lastName}`} image={user.image} />
            <Section sectionName="Educación" elements={user.education} />
            <Section sectionName="Experiencia" elements={user.experience} />
          </div>
        }


        <button onClick={this.setIsEditing}>
          {this.state.isEditing ? 'Cerrar' : 'Editar Perfil'}
        </button>
      </div>
    );
  }
}

export default App;
