import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <header className="header">
                <img src={this.props.image} alt="user" style={{ width: '240px', border: '2px solid black' }}/>
                <h1>{this.props.fullName}</h1>
            </header>
        )
    }
}

export default Header;