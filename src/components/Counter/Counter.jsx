import React, { Component } from 'react';

class Counter extends Component {
    // Forma corta de definir el estado
    // state = {
    //     counter: 0,
    // }
   //Crear un constructor para crear un estado
    constructor(props) {
        super(props);

        this.state = {
            counter: props.startCount,
        }
    };

    decrement = () => {
        console.log('ev decrement');

        this.setState({
            counter: this.state.counter - 1,
        });
    };

    increment = () => {
        console.log('ev increment');

        this.setState({
            counter: this.state.counter + 1,
        });
    };

    render() {
        return (
            <div style={{marginBottom: '8opx'}}>
                <h2>El contador está en -- {this.state.counter} -- </h2>
                <button onClick={this.decrement} >Restar</button>
                <button onClick={this.increment}>Sumar</button>
            </div>
        );
    };
};

export default Counter;