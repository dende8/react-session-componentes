import React, { Component } from 'react';

class Section extends Component {
    render() {
        return (
            <section className="header">
                <h3>{this.props.sectionName}</h3>

                <ul>
                    {this.props.elements.map(el => {
                        return (
                            <li key={JSON.stringify(el)}>
                                <p>Título: {el.title}</p>
                                <p>Empresa: {el.company}</p>
                            </li>
                        )
                    })}
                </ul>
                <hr />
            </section>
        );
    }

};

export default Section;