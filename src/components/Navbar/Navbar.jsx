import React from 'react';
import './Navbar.scss';

const language = 'es';
const spanishTitle = 'Bienvenidos a React';
const englishTitle = 'Welcome to React';

const links = ['Home', 'About', 'Account'];

class Navbar extends React.Component {
    render() {
        return (
            <nav className="nav">
                <h1>{language === 'es' ? spanishTitle : englishTitle}</h1>

                <ul>
                    {links.map((link) => {
                        return <li key={link}>{link}</li>
                    })}
                </ul>
            </nav>
        );
    }
};

export default Navbar;