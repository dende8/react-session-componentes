import React, { Component } from 'react';


class Form extends Component {

    submitForm = (event) => {
        event.preventDefault();
    };

    changeInput = (ev) => {
        this.props.changeUserFirstName(ev.target.value);
    };
    render() {
        
        return (
            <form onSumbit={this.submitForm}>
                <label>
                    <p>Nombre:</p>
                    <input type="text" name="name" value={this.props.user.firstName} onChange={this.changeInput} />
                </label>
            </form>
        )
    }
}

export default Form;